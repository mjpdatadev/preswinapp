﻿namespace PresEntry
{
    partial class frmLogin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtEmailAddr = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtPasswd = new System.Windows.Forms.TextBox();
            this.btnLogin = new System.Windows.Forms.Button();
            this.btnAddNew = new System.Windows.Forms.Button();
            this.btnLogExit = new System.Windows.Forms.Button();
            this.lblLogMsg = new System.Windows.Forms.Label();
            this.bntForgotUsrNm = new System.Windows.Forms.Button();
            this.btnForgotPWD = new System.Windows.Forms.Button();
            this.btnChangePWD = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtEmailAddr
            // 
            this.txtEmailAddr.Location = new System.Drawing.Point(94, 12);
            this.txtEmailAddr.Name = "txtEmailAddr";
            this.txtEmailAddr.Size = new System.Drawing.Size(186, 20);
            this.txtEmailAddr.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Email Address:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 46);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Password:";
            // 
            // txtPasswd
            // 
            this.txtPasswd.Location = new System.Drawing.Point(94, 41);
            this.txtPasswd.Name = "txtPasswd";
            this.txtPasswd.PasswordChar = '*';
            this.txtPasswd.Size = new System.Drawing.Size(186, 20);
            this.txtPasswd.TabIndex = 2;
            // 
            // btnLogin
            // 
            this.btnLogin.Location = new System.Drawing.Point(94, 91);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Size = new System.Drawing.Size(55, 25);
            this.btnLogin.TabIndex = 4;
            this.btnLogin.Text = "Login";
            this.btnLogin.UseVisualStyleBackColor = true;
            this.btnLogin.Click += new System.EventHandler(this.btnLogin_Click);
            // 
            // btnAddNew
            // 
            this.btnAddNew.Location = new System.Drawing.Point(61, 122);
            this.btnAddNew.Name = "btnAddNew";
            this.btnAddNew.Size = new System.Drawing.Size(88, 25);
            this.btnAddNew.TabIndex = 5;
            this.btnAddNew.Text = "Add New User";
            this.btnAddNew.UseVisualStyleBackColor = true;
            this.btnAddNew.Visible = false;
            this.btnAddNew.Click += new System.EventHandler(this.btnAddNew_Click);
            // 
            // btnLogExit
            // 
            this.btnLogExit.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnLogExit.Location = new System.Drawing.Point(155, 91);
            this.btnLogExit.Name = "btnLogExit";
            this.btnLogExit.Size = new System.Drawing.Size(55, 25);
            this.btnLogExit.TabIndex = 6;
            this.btnLogExit.Text = "Close";
            this.btnLogExit.UseVisualStyleBackColor = true;
            this.btnLogExit.Click += new System.EventHandler(this.btnLogExit_Click);
            // 
            // lblLogMsg
            // 
            this.lblLogMsg.AutoSize = true;
            this.lblLogMsg.Location = new System.Drawing.Point(14, 72);
            this.lblLogMsg.Name = "lblLogMsg";
            this.lblLogMsg.Size = new System.Drawing.Size(72, 13);
            this.lblLogMsg.TabIndex = 7;
            this.lblLogMsg.Text = "Please Log In";
            // 
            // bntForgotUsrNm
            // 
            this.bntForgotUsrNm.Location = new System.Drawing.Point(74, 153);
            this.bntForgotUsrNm.Name = "bntForgotUsrNm";
            this.bntForgotUsrNm.Size = new System.Drawing.Size(75, 23);
            this.bntForgotUsrNm.TabIndex = 8;
            this.bntForgotUsrNm.Text = "Forgot Usr";
            this.bntForgotUsrNm.UseVisualStyleBackColor = true;
            this.bntForgotUsrNm.Click += new System.EventHandler(this.bntForgotUsrNm_Click);
            // 
            // btnForgotPWD
            // 
            this.btnForgotPWD.Location = new System.Drawing.Point(155, 153);
            this.btnForgotPWD.Name = "btnForgotPWD";
            this.btnForgotPWD.Size = new System.Drawing.Size(75, 23);
            this.btnForgotPWD.TabIndex = 9;
            this.btnForgotPWD.Text = "Forgot PWD";
            this.btnForgotPWD.UseVisualStyleBackColor = true;
            this.btnForgotPWD.Click += new System.EventHandler(this.btnForgotPWD_Click);
            // 
            // btnChangePWD
            // 
            this.btnChangePWD.Location = new System.Drawing.Point(155, 124);
            this.btnChangePWD.Name = "btnChangePWD";
            this.btnChangePWD.Size = new System.Drawing.Size(88, 23);
            this.btnChangePWD.TabIndex = 10;
            this.btnChangePWD.Text = "Change PWD";
            this.btnChangePWD.UseVisualStyleBackColor = true;
            this.btnChangePWD.Click += new System.EventHandler(this.btnChangePWD_Click);
            // 
            // frmLogin
            // 
            this.AcceptButton = this.btnLogin;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnLogExit;
            this.ClientSize = new System.Drawing.Size(292, 189);
            this.ControlBox = false;
            this.Controls.Add(this.btnChangePWD);
            this.Controls.Add(this.btnForgotPWD);
            this.Controls.Add(this.bntForgotUsrNm);
            this.Controls.Add(this.lblLogMsg);
            this.Controls.Add(this.btnLogExit);
            this.Controls.Add(this.btnAddNew);
            this.Controls.Add(this.btnLogin);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtPasswd);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtEmailAddr);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "frmLogin";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Please Login";
            this.Load += new System.EventHandler(this.frmLogin_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtEmailAddr;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtPasswd;
        private System.Windows.Forms.Button btnLogin;
        private System.Windows.Forms.Button btnAddNew;
        private System.Windows.Forms.Button btnLogExit;
        private System.Windows.Forms.Label lblLogMsg;
        private System.Windows.Forms.Button bntForgotUsrNm;
        private System.Windows.Forms.Button btnForgotPWD;
        private System.Windows.Forms.Button btnChangePWD;
    }
}