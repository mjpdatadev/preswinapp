﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace PresEntry
{
    public partial class frmNewUser : Form
    {
        #region //===============================  Property  UserNo  ====================================
        private int user_no;
        public int UsrNumber
        {
            get
            {
                return user_no;
            }
            set
            {
                user_no = value;
            }
        }
        #endregion  //===============================  End Property UserNo  ====================================
        #region //===============================  Property  ForgotWhich  ====================================
        private int which_lost;
        public int ForgotWhich
        {
            get
            {
                return which_lost;
            }
            set
            {
                which_lost = value;
            }
        }
        #endregion  //===============================  End Property ForgotWhich  ====================================
        public frmNewUser()
        {
            InitializeComponent();
        }

        private void btnUserSave_Click(object sender, EventArgs e)
        {
            /*Use the reset function based on this.ForgotWhich 
                0=Add New User
                1=Forgot username
                2=Forgot password
                3=Change my password only
            */
            if (this.ForgotWhich == 0)
            {
                AddNewUser();
            }
            else
            {
                ResetPassword();
            }
        }

        private bool AddNewUser()
        {
            bool userAdded = false;
            string usrParams = String.Empty;
            String showMsg = String.Empty;
            String showTitle = String.Empty;
            String sEncrptPWD = String.Empty;

            if (IsValidNewLogin())
            {
                EncDecryp encPass = new EncDecryp();
                sEncrptPWD = encPass.Encrypt(txtPWDConfirm.Text);

                usrParams = txtUsrFName.Text + "~" +
                txtUsrLName.Text + "~" +
                    txtEmailAddr.Text + "~" +
                    sEncrptPWD + "~" +
                    ((chkMakeUsers.Checked)?"1":"0") + "~" +
                    ((chkChngPWD.Checked) ? "1" : "0") + "~" +
                    this.UsrNumber;

                usrParams = usrParams.Replace("'", "''");  //<---- check for apostrophes
                usrParams = usrParams.Replace("~~", "~''~");    //<------  replace null fields with empty strings 
                usrParams = usrParams.Replace("~", ",");   //<------  replace tilda with commas for the sproc

               ExecStProc xspAddUser = new ExecStProc();
               SqlDataReader rdrNewUsr = xspAddUser.CreateSpReader("uspLoginAdd(" + usrParams + ")", "");
                if (rdrNewUsr.HasRows)
                {
                    rdrNewUsr.Read();
                    String retrnMsg = String.Empty;
                    String retrnCode = String.Empty;
                    retrnMsg = rdrNewUsr[0].ToString();
                    retrnCode = rdrNewUsr[1].ToString();

                    if (retrnMsg.IndexOf("Success") > -1)
                    {
                        showMsg = "User added successfully";
                        showTitle = "New User";
                        userAdded = true;
                    }
                    else if (retrnMsg.IndexOf("Exist") > -1)
                    {
                        showMsg = "This user already exists";
                        showTitle = "Existing User";
                        userAdded = false;
                    }
                    else if (retrnMsg.IndexOf("Error") > -1)
                    {
                        showMsg = "A network or sql error occurred. Error: " + retrnCode;
                        showTitle = "User Not Added";
                        userAdded = false;
                    }
                }
            }
            else
            {
                userAdded = false;
            }

            MessageBox.Show(showMsg, showTitle, MessageBoxButtons.OK);
            bool isCleared = ClearForm_NewUser();
            return userAdded;
        }
        #region //===============================  Property  IsValidNewLogin  ==================================================================
        private bool IsValidNewLogin()
        {
            bool isValidNew = true;

            if (txtUsrFName.Text.Trim().Length < 1 && txtUsrFName.Enabled== true)
            { 
                MessageBox.Show("The user first name is required.", "Required Field", MessageBoxButtons.OK);
                txtUsrFName.Focus();
                isValidNew = false;
                goto retInvalid;
            }

            if (txtUsrLName.Text.Trim().Length < 1 && txtUsrLName.Enabled == true)
            {
                MessageBox.Show("The user last name is required.", "Required Field", MessageBoxButtons.OK);
                txtUsrLName.Focus();
                isValidNew = false;
                goto retInvalid;
            }

            if (txtEmailAddr.Text.Trim().Length < 1 && txtEmailAddr.Enabled== true)
            {
                MessageBox.Show("The user's email address is required.", "Required Field", MessageBoxButtons.OK);
                txtEmailAddr.Focus();
                isValidNew = false;
                goto retInvalid;
            }

            if (txtEmailAddr.Enabled == true)
            {
                ReUse EMChk = new ReUse();
                bool emailPassed = EMChk.IsValidEmail(txtEmailAddr.Text.Trim());
                if (!emailPassed)
                {
                    MessageBox.Show("The user's email address is in an incorrect format.", "Invalid Email Address", MessageBoxButtons.OK);
                    txtEmailAddr.Focus();
                    isValidNew = false;
                    goto retInvalid;
                }
            }

            if (txtPasswd1.Text.Trim().Length < 1 && txtPasswd1.Enabled== true)
            {
                MessageBox.Show("A user's password is required.", "Required Field", MessageBoxButtons.OK);
                txtPasswd1.Focus();
                isValidNew = false;
                goto retInvalid;
            }

            if (txtPWDConfirm.Text.Trim().Length < 1 && txtPWDConfirm.Enabled == true)
            {
                MessageBox.Show("A matching password is required.", "Required Field", MessageBoxButtons.OK);
                txtPWDConfirm.Focus();
                isValidNew = false;
                goto retInvalid;
            }

            if ((txtPWDConfirm.Text!= txtPasswd1.Text) && txtPasswd1.Enabled == true)
            {
                MessageBox.Show("Both passwords must match", "Required Field", MessageBoxButtons.OK);
                txtPWDConfirm.Focus();
                isValidNew = false;
                goto retInvalid;
            }

        retInvalid:
            return isValidNew;
        }
        #endregion //===============================  Property  IsValidNewLogin  ==================================================================

        #region //===============================  Property  frmNewUser_Load  ==================================================================
        private void frmNewUser_Load(object sender, EventArgs e)
        {
            if (chkShow.Checked == true) {
                txtPasswd1.PasswordChar = '\0';
            txtPWDConfirm.PasswordChar = '\0';
            }
            if (this.ForgotWhich > 0)
            {
                if (this.ForgotWhich == 1) //--<----- fogot username.
                {
                    this.Text = "Enter User Credentials";
                    txtEmailAddr.Enabled = false;
                    txtPasswd1.Enabled = false;
                    txtPWDConfirm.Enabled = false;
                    chkMakeUsers.Enabled = false;
                    chkChngPWD.Enabled = false;
                    MessageBox.Show("Please enter your first name and last name. Then click save.\r\n If your name exists in our system then your password will be reset and an email that contains your username and your new password will be sent to you.","Enter Credentials", MessageBoxButtons.OK);
                }
                if (this.ForgotWhich == 2) //--<----- fogot password.
                {
                    this.Text = "Enter User Credentials";
                    txtPasswd1.Enabled = false;
                    txtPWDConfirm.Enabled = false;
                    chkMakeUsers.Enabled = false;
                    chkChngPWD.Enabled = false;
                    MessageBox.Show("Please enter your first name, last name, and email address.\r\n Then click save. If your name exists in our system then your password will be reset and an email that contains your username and your new password will be sent to you.", "Enter Credentials", MessageBoxButtons.OK);
                }
                if (this.ForgotWhich == 3) //--<----- change password.
                {
                    this.Text = "Enter User Credentials";
                    txtUsrFName.Enabled = false;
                    txtUsrLName.Enabled = false;
                    txtEmailAddr.Enabled = false;
                    chkMakeUsers.Enabled = false;
                    chkChngPWD.Enabled = false;
                    MessageBox.Show("Please enter your new password in both boxes. Then click save.\r\n If your name exists in our system then your password will be reset and an email that contains your username and your new password will be sent to you.", "Enter Credentials", MessageBoxButtons.OK);
                }
            }
        }

        private void chkShow_CheckedChanged(object sender, EventArgs e)
        {
            if (chkShow.Checked == true)
            {
                txtPasswd1.PasswordChar = '\0';
                txtPWDConfirm.PasswordChar = '\0';
            }
            else
            {
                txtPasswd1.PasswordChar = Convert.ToChar("*");
                txtPWDConfirm.PasswordChar = Convert.ToChar("*");
            }
        }

        private void btnSendMail_Click(object sender, EventArgs e)
        {
            string fromSender = "";
            string toRecip = "mart01@mjpdata.com";
            string strTitle = "This is a Test from the App";
            string strBody = "I am testing this app using my passthru account in gmail.";

            XMLConfig xmlGetSet = new XMLConfig();
            fromSender = xmlGetSet.GetSetting("EmailSettings:FromAddr");

            clsSendMail testSender = new clsSendMail(fromSender, toRecip);
            testSender.SendEmail(strTitle, strBody);
        }
        #endregion //===============================  Property  frmNewUser_Load  ==================================================================

        #region  //======================================================  Begin ForgotPassword ===================================================
        /// <summary>
        /// Needed:  First Name,   Last Name, Password
        /// </summary>
        /// <returns></returns>
        private bool ResetPassword()
        {
            bool fixedPWD = false;
            string strFPWD = String.Empty;
            int userNo = -1;
            string emailAdr = String.Empty;
            int ChangeType = this.ForgotWhich;

            if (IsValidNewLogin())
            {
                strFPWD = txtUsrFName.Text.Trim() + "~" +
                    txtUsrLName.Text.Trim() + "~" +
                    txtEmailAddr.Text.Trim() + "~" +
                    this.UsrNumber.ToString();

                strFPWD = strFPWD.Replace("'", "''");  //<---- check for apostrophes
                strFPWD = strFPWD.Replace("~~", "~''~");    //<------  replace null fields with empty strings 
                strFPWD = strFPWD.Replace("~~", "~''~");    //<------  replace null fields with empty strings 
                strFPWD = strFPWD.Replace("~", ",");   //<------  replace tilda with commas for the sproc

                if (strFPWD.Substring(0, 1) == ",")
                {
                    strFPWD = "'1'" + strFPWD;
                }
                strFPWD = strFPWD.Replace("'", "");


                ExecStProc xspFgotPWD = new ExecStProc();
                SqlDataReader rdrFgotPWD = xspFgotPWD.CreateSpReader("uspLoginFXUser(" + strFPWD + ")", "");
                if (rdrFgotPWD.HasRows)
                {
                    rdrFgotPWD.Read();
                    if (rdrFgotPWD[0].ToString().IndexOf("Error") > -1)
                    {
                        MessageBox.Show("User was not found on this system", "No User", MessageBoxButtons.OK);
                        this.Close();
                    }
                    else
                    {
                        if (Int32.Parse(rdrFgotPWD[0].ToString())>0)
                        {
                            string strResetTitle = String.Empty;
                            string strResetMsg = String.Empty;
                            int iMustChng = (ChangeType==3) ? 0 : 1;  //--<--- do they have to change pwd at next login?
                            userNo = Int32.Parse(rdrFgotPWD[0].ToString());
                            emailAdr= rdrFgotPWD[1].ToString();
                            string[] strChangePWD;
                            strChangePWD = new string[2];
                            if (ChangeType==3)
                            {
                                EncDecryp encTypedPWD = new EncDecryp();
                                strChangePWD[0] = "notused";
                                strChangePWD[1] = encTypedPWD.Encrypt(txtPWDConfirm.Text);
                            }
                            else {
                                ReUse newGetPass = new ReUse();
                                string[] strGetPass = newGetPass.NewPasswrd();
                                strChangePWD[0] = strGetPass[0];
                                strChangePWD[1] = strGetPass[1];
                            }
                            if (!rdrFgotPWD.IsClosed) { rdrFgotPWD.Close(); }
                            strFPWD = "";
                            strFPWD = userNo + "~" +
                                emailAdr + "~" +
                                strChangePWD[1] + "~" +
                                iMustChng.ToString() + "~" +
                                this.UsrNumber.ToString();
                            strFPWD = strFPWD.Replace("'", "''");  //<---- check for apostrophes
                            strFPWD = strFPWD.Replace("~~", "~''~");    //<------  replace null fields with empty strings 
                            strFPWD = strFPWD.Replace("~", ",");   //<------  replace tilda with commas for the sproc

                          //  MessageBox.Show(strChangePWD[0]);

                            rdrFgotPWD = xspFgotPWD.CreateSpReader("uspLoginSaveNewPWD(" + strFPWD + ")", "");

                            if (rdrFgotPWD.HasRows)
                            {
                                rdrFgotPWD.Read();
                                if (rdrFgotPWD[0].ToString().IndexOf("Success") > -1)
                                {
                                    MessageBox.Show("Password Change Successful", "Password Success", MessageBoxButtons.OK);
                                    if (ChangeType!=0)
                                    {
                                        strResetTitle = "Roper Center Login Reset";
                                        strResetMsg = "Your password has been reset.\r\nPlease check your email then login using your new password.\r\n Your new password is:\r\n" +
                                            strChangePWD[0];
                                        clsSendMail emSender = new clsSendMail("relayserver@cornell.edu", emailAdr);
                                        // unrem LATER!!!   emSender.SendEmail(strResetTitle, strResetMsg);
                                    }
                                    this.Close();
                                    this.Dispose();
                                }
                                else
                                {
                                    MessageBox.Show("Your password change was not successful. Please try again.", "Update Failed", MessageBoxButtons.OK);
                                }
                            }
                            else
                            {
                                MessageBox.Show("Your password change was not successful. Please try again.", "Update Failed", MessageBoxButtons.OK);
                            }
                        }
                    }
                }
            }

                return fixedPWD;
        }
        #endregion //==================================================  ***END*** ForgotPassword ===================================================

        #region  //======================================================  Begin ForgotUserName ===================================================
        private bool ForgotUserName()
        {
            bool fixedUser = false;

            //---THe process is exactly the same as forgetting your password so use the same method\function. The only
            //--- difference is that the stored proc will use only your FName and LName to find you.
          ///  ForgotPassword(false);

            return fixedUser;
        }
        #endregion //==================================================  ***END*** ForgotUserName ===================================================

        #region  //======================================================  Begin ChangePassword ===================================================
        private bool ChangePassword()
        {
            bool chngPWD = false;

            //---THe process is exactly the same as forgetting your password so use the same method\function. The only
            //--- difference is that you get to use the password you chose in the textbox.
         //   ForgotPassword(true);

            return chngPWD;
        }
        #endregion //==================================================  ***END*** ChangePassword ===================================================
        #region //===============================  Property  frmNewUser_Load  ==================================================================
        private bool ClearForm_NewUser()
        {
            bool formCleared = false;
            try
            {
                txtUsrFName.Text = "";
                txtUsrLName.Text = "";
                txtEmailAddr.Text = "";
                txtPasswd1.Text = "";
                txtPWDConfirm.Text = "";
                chkShow.Checked = true;
                chkChngPWD.Checked = false;
                chkMakeUsers.Checked = false;
                formCleared = true;
            }
            catch
            {
                MessageBox.Show(this, "There was an issue resetting the page.", "Page Not Cleared", MessageBoxButtons.OK);
            }
            return formCleared;
        }
        #endregion //===============================  Property  frmNewUser_Load  ==================================================================

    }
}
