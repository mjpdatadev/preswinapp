﻿namespace PresEntry
{
    partial class frmPresEntry
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPresEntry));
            this.pnlPolls = new System.Windows.Forms.Panel();
            this.dtimEnd = new System.Windows.Forms.DateTimePicker();
            this.dtimBegin = new System.Windows.Forms.DateTimePicker();
            this.cmbTerm = new System.Windows.Forms.ComboBox();
            this.cmbPres = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cmbPollOrg = new System.Windows.Forms.ComboBox();
            this.grpSamples = new System.Windows.Forms.GroupBox();
            this.txtNoOpin = new System.Windows.Forms.TextBox();
            this.txtDisappove = new System.Windows.Forms.TextBox();
            this.txtApprove = new System.Windows.Forms.TextBox();
            this.txtSample = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.pnlDates = new System.Windows.Forms.Panel();
            this.lblLastUser = new System.Windows.Forms.Label();
            this.lblLastUpdt = new System.Windows.Forms.Label();
            this.lblCreatedOn = new System.Windows.Forms.Label();
            this.lblLoginName = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.picLogin = new System.Windows.Forms.PictureBox();
            this.pnlControls = new System.Windows.Forms.Panel();
            this.btnNew = new System.Windows.Forms.Button();
            this.btnLast = new System.Windows.Forms.Button();
            this.btnNext = new System.Windows.Forms.Button();
            this.txtGoTo = new System.Windows.Forms.TextBox();
            this.btnPrev = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnGoto = new System.Windows.Forms.Button();
            this.btnFirst = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.lblRecNo = new System.Windows.Forms.Label();
            this.txtNotes = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.pnlPolls.SuspendLayout();
            this.grpSamples.SuspendLayout();
            this.pnlDates.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picLogin)).BeginInit();
            this.pnlControls.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlPolls
            // 
            this.pnlPolls.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlPolls.Controls.Add(this.dtimEnd);
            this.pnlPolls.Controls.Add(this.dtimBegin);
            this.pnlPolls.Controls.Add(this.cmbTerm);
            this.pnlPolls.Controls.Add(this.cmbPres);
            this.pnlPolls.Controls.Add(this.label5);
            this.pnlPolls.Controls.Add(this.label4);
            this.pnlPolls.Controls.Add(this.label3);
            this.pnlPolls.Controls.Add(this.label2);
            this.pnlPolls.Controls.Add(this.label1);
            this.pnlPolls.Controls.Add(this.cmbPollOrg);
            this.pnlPolls.Location = new System.Drawing.Point(26, 36);
            this.pnlPolls.Name = "pnlPolls";
            this.pnlPolls.Size = new System.Drawing.Size(478, 158);
            this.pnlPolls.TabIndex = 24;
            // 
            // dtimEnd
            // 
            this.dtimEnd.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtimEnd.Location = new System.Drawing.Point(369, 126);
            this.dtimEnd.Name = "dtimEnd";
            this.dtimEnd.Size = new System.Drawing.Size(92, 20);
            this.dtimEnd.TabIndex = 5;
            this.dtimEnd.ValueChanged += new System.EventHandler(this.dtimEnd_ValueChanged);
            // 
            // dtimBegin
            // 
            this.dtimBegin.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtimBegin.Location = new System.Drawing.Point(136, 125);
            this.dtimBegin.Name = "dtimBegin";
            this.dtimBegin.Size = new System.Drawing.Size(92, 20);
            this.dtimBegin.TabIndex = 4;
            this.dtimBegin.ValueChanged += new System.EventHandler(this.dtimBegin_ValueChanged);
            // 
            // cmbTerm
            // 
            this.cmbTerm.FormattingEnabled = true;
            this.cmbTerm.Location = new System.Drawing.Point(136, 84);
            this.cmbTerm.Name = "cmbTerm";
            this.cmbTerm.Size = new System.Drawing.Size(182, 21);
            this.cmbTerm.TabIndex = 3;
            // 
            // cmbPres
            // 
            this.cmbPres.FormattingEnabled = true;
            this.cmbPres.Location = new System.Drawing.Point(136, 50);
            this.cmbPres.Name = "cmbPres";
            this.cmbPres.Size = new System.Drawing.Size(227, 21);
            this.cmbPres.TabIndex = 2;
            this.cmbPres.SelectedIndexChanged += new System.EventHandler(this.cmbPres_SelectedIndexChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(262, 132);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(101, 13);
            this.label5.TabIndex = 22;
            this.label5.Text = "Ending Date of Poll:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(15, 131);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(115, 13);
            this.label4.TabIndex = 21;
            this.label4.Text = "Beginning Date of Poll:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(15, 92);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 13);
            this.label3.TabIndex = 20;
            this.label3.Text = "Term In Office:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 58);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 13);
            this.label2.TabIndex = 19;
            this.label2.Text = "President:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(103, 13);
            this.label1.TabIndex = 18;
            this.label1.Text = "Polling Organization:";
            // 
            // cmbPollOrg
            // 
            this.cmbPollOrg.FormattingEnabled = true;
            this.cmbPollOrg.Location = new System.Drawing.Point(135, 16);
            this.cmbPollOrg.Name = "cmbPollOrg";
            this.cmbPollOrg.Size = new System.Drawing.Size(326, 21);
            this.cmbPollOrg.TabIndex = 1;
            this.cmbPollOrg.SelectedIndexChanged += new System.EventHandler(this.cmbPollOrg_SelectedIndexChanged);
            // 
            // grpSamples
            // 
            this.grpSamples.Controls.Add(this.txtNoOpin);
            this.grpSamples.Controls.Add(this.txtDisappove);
            this.grpSamples.Controls.Add(this.txtApprove);
            this.grpSamples.Controls.Add(this.txtSample);
            this.grpSamples.Controls.Add(this.label9);
            this.grpSamples.Controls.Add(this.label8);
            this.grpSamples.Controls.Add(this.label7);
            this.grpSamples.Controls.Add(this.label6);
            this.grpSamples.Location = new System.Drawing.Point(26, 199);
            this.grpSamples.Name = "grpSamples";
            this.grpSamples.Size = new System.Drawing.Size(174, 125);
            this.grpSamples.TabIndex = 25;
            this.grpSamples.TabStop = false;
            this.grpSamples.Text = "Sample Data";
            // 
            // txtNoOpin
            // 
            this.txtNoOpin.Location = new System.Drawing.Point(102, 98);
            this.txtNoOpin.Name = "txtNoOpin";
            this.txtNoOpin.Size = new System.Drawing.Size(48, 20);
            this.txtNoOpin.TabIndex = 9;
            this.txtNoOpin.Text = "0";
            this.txtNoOpin.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtNoOpin.Leave += new System.EventHandler(this.txtNoOpin_Leave);
            // 
            // txtDisappove
            // 
            this.txtDisappove.Location = new System.Drawing.Point(103, 72);
            this.txtDisappove.Name = "txtDisappove";
            this.txtDisappove.Size = new System.Drawing.Size(48, 20);
            this.txtDisappove.TabIndex = 8;
            this.txtDisappove.Text = "0";
            this.txtDisappove.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDisappove.Leave += new System.EventHandler(this.txtDisappove_Leave);
            // 
            // txtApprove
            // 
            this.txtApprove.Location = new System.Drawing.Point(103, 45);
            this.txtApprove.Name = "txtApprove";
            this.txtApprove.Size = new System.Drawing.Size(48, 20);
            this.txtApprove.TabIndex = 7;
            this.txtApprove.Text = "0";
            this.txtApprove.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtApprove.Leave += new System.EventHandler(this.txtApprove_Leave);
            // 
            // txtSample
            // 
            this.txtSample.Location = new System.Drawing.Point(104, 18);
            this.txtSample.Name = "txtSample";
            this.txtSample.Size = new System.Drawing.Size(48, 20);
            this.txtSample.TabIndex = 6;
            this.txtSample.Text = "0";
            this.txtSample.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtSample.Leave += new System.EventHandler(this.txtSample_Leave);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(19, 101);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(63, 13);
            this.label9.TabIndex = 24;
            this.label9.Text = "No Opinion:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(18, 75);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(64, 13);
            this.label8.TabIndex = 23;
            this.label8.Text = "Disapprove:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(18, 46);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(50, 13);
            this.label7.TabIndex = 22;
            this.label7.Text = "Approve:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(18, 21);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(68, 13);
            this.label6.TabIndex = 21;
            this.label6.Text = "Sample Size:";
            // 
            // pnlDates
            // 
            this.pnlDates.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlDates.Controls.Add(this.lblLastUser);
            this.pnlDates.Controls.Add(this.lblLastUpdt);
            this.pnlDates.Controls.Add(this.lblCreatedOn);
            this.pnlDates.Location = new System.Drawing.Point(26, 323);
            this.pnlDates.Name = "pnlDates";
            this.pnlDates.Size = new System.Drawing.Size(478, 46);
            this.pnlDates.TabIndex = 26;
            // 
            // lblLastUser
            // 
            this.lblLastUser.AutoSize = true;
            this.lblLastUser.BackColor = System.Drawing.SystemColors.Control;
            this.lblLastUser.Location = new System.Drawing.Point(4, 26);
            this.lblLastUser.Name = "lblLastUser";
            this.lblLastUser.Size = new System.Drawing.Size(138, 13);
            this.lblLastUser.TabIndex = 26;
            this.lblLastUser.Text = "Last Updated By: Jane Doe";
            // 
            // lblLastUpdt
            // 
            this.lblLastUpdt.AutoSize = true;
            this.lblLastUpdt.Location = new System.Drawing.Point(326, 6);
            this.lblLastUpdt.Name = "lblLastUpdt";
            this.lblLastUpdt.Size = new System.Drawing.Size(141, 13);
            this.lblLastUpdt.TabIndex = 25;
            this.lblLastUpdt.Text = "Last Updated:   03/21/2016";
            // 
            // lblCreatedOn
            // 
            this.lblCreatedOn.AutoSize = true;
            this.lblCreatedOn.BackColor = System.Drawing.SystemColors.Control;
            this.lblCreatedOn.Location = new System.Drawing.Point(3, 6);
            this.lblCreatedOn.Name = "lblCreatedOn";
            this.lblCreatedOn.Size = new System.Drawing.Size(131, 13);
            this.lblCreatedOn.TabIndex = 24;
            this.lblCreatedOn.Text = "Created On:   03/21/2016";
            // 
            // lblLoginName
            // 
            this.lblLoginName.AutoSize = true;
            this.lblLoginName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLoginName.Location = new System.Drawing.Point(57, 13);
            this.lblLoginName.Name = "lblLoginName";
            this.lblLoginName.Size = new System.Drawing.Size(143, 13);
            this.lblLoginName.TabIndex = 27;
            this.lblLoginName.Tag = "Error";
            this.lblLoginName.Text = "<<<------ Click To Log In";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(423, 13);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(47, 13);
            this.label14.TabIndex = 30;
            this.label14.Text = "Rec No:";
            // 
            // picLogin
            // 
            this.picLogin.Image = ((System.Drawing.Image)(resources.GetObject("picLogin.Image")));
            this.picLogin.Location = new System.Drawing.Point(26, 5);
            this.picLogin.Name = "picLogin";
            this.picLogin.Size = new System.Drawing.Size(25, 25);
            this.picLogin.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picLogin.TabIndex = 37;
            this.picLogin.TabStop = false;
            this.picLogin.Click += new System.EventHandler(this.picLogin_Click);
            // 
            // pnlControls
            // 
            this.pnlControls.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlControls.Controls.Add(this.btnNew);
            this.pnlControls.Controls.Add(this.btnLast);
            this.pnlControls.Controls.Add(this.btnNext);
            this.pnlControls.Controls.Add(this.txtGoTo);
            this.pnlControls.Controls.Add(this.btnPrev);
            this.pnlControls.Controls.Add(this.btnSave);
            this.pnlControls.Controls.Add(this.btnGoto);
            this.pnlControls.Controls.Add(this.btnFirst);
            this.pnlControls.Location = new System.Drawing.Point(88, 375);
            this.pnlControls.Name = "pnlControls";
            this.pnlControls.Size = new System.Drawing.Size(332, 85);
            this.pnlControls.TabIndex = 38;
            // 
            // btnNew
            // 
            this.btnNew.Location = new System.Drawing.Point(172, 4);
            this.btnNew.Name = "btnNew";
            this.btnNew.Size = new System.Drawing.Size(75, 23);
            this.btnNew.TabIndex = 12;
            this.btnNew.Text = "New";
            this.btnNew.UseVisualStyleBackColor = true;
            this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // btnLast
            // 
            this.btnLast.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLast.Location = new System.Drawing.Point(253, 33);
            this.btnLast.Name = "btnLast";
            this.btnLast.Size = new System.Drawing.Size(58, 23);
            this.btnLast.TabIndex = 17;
            this.btnLast.Text = "Last>>";
            this.btnLast.UseVisualStyleBackColor = true;
            this.btnLast.Click += new System.EventHandler(this.btnLast_Click);
            // 
            // btnNext
            // 
            this.btnNext.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNext.Location = new System.Drawing.Point(192, 33);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(58, 23);
            this.btnNext.TabIndex = 16;
            this.btnNext.Text = "Next >";
            this.btnNext.UseVisualStyleBackColor = true;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // txtGoTo
            // 
            this.txtGoTo.Location = new System.Drawing.Point(139, 33);
            this.txtGoTo.Name = "txtGoTo";
            this.txtGoTo.Size = new System.Drawing.Size(44, 20);
            this.txtGoTo.TabIndex = 15;
            this.txtGoTo.Text = "0";
            this.txtGoTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtGoTo.Enter += new System.EventHandler(this.txtGoTo_Enter);
            this.txtGoTo.Leave += new System.EventHandler(this.txtGoTo_Leave);
            this.txtGoTo.MouseUp += new System.Windows.Forms.MouseEventHandler(this.txtGoTo_MouseUp);
            // 
            // btnPrev
            // 
            this.btnPrev.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrev.Location = new System.Drawing.Point(74, 32);
            this.btnPrev.Name = "btnPrev";
            this.btnPrev.Size = new System.Drawing.Size(58, 23);
            this.btnPrev.TabIndex = 14;
            this.btnPrev.Text = "< Prev";
            this.btnPrev.UseVisualStyleBackColor = true;
            this.btnPrev.Click += new System.EventHandler(this.btnPrev_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(91, 4);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 11;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnGoto
            // 
            this.btnGoto.Location = new System.Drawing.Point(136, 56);
            this.btnGoto.Name = "btnGoto";
            this.btnGoto.Size = new System.Drawing.Size(49, 23);
            this.btnGoto.TabIndex = 18;
            this.btnGoto.Text = "Go To";
            this.btnGoto.UseVisualStyleBackColor = true;
            this.btnGoto.Click += new System.EventHandler(this.btnGoto_Click);
            // 
            // btnFirst
            // 
            this.btnFirst.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFirst.Location = new System.Drawing.Point(14, 32);
            this.btnFirst.Name = "btnFirst";
            this.btnFirst.Size = new System.Drawing.Size(58, 23);
            this.btnFirst.TabIndex = 13;
            this.btnFirst.Text = "<<First";
            this.btnFirst.UseVisualStyleBackColor = true;
            this.btnFirst.Click += new System.EventHandler(this.btnFirst_Click);
            // 
            // btnExit
            // 
            this.btnExit.Location = new System.Drawing.Point(429, 383);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(75, 23);
            this.btnExit.TabIndex = 19;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // lblRecNo
            // 
            this.lblRecNo.AutoSize = true;
            this.lblRecNo.Location = new System.Drawing.Point(470, 12);
            this.lblRecNo.Name = "lblRecNo";
            this.lblRecNo.Size = new System.Drawing.Size(16, 13);
            this.lblRecNo.TabIndex = 39;
            this.lblRecNo.Text = "-9";
            this.lblRecNo.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.lblRecNo.TextChanged += new System.EventHandler(this.lblRecNo_TextChanged);
            // 
            // txtNotes
            // 
            this.txtNotes.Enabled = false;
            this.txtNotes.Location = new System.Drawing.Point(225, 217);
            this.txtNotes.Multiline = true;
            this.txtNotes.Name = "txtNotes";
            this.txtNotes.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtNotes.Size = new System.Drawing.Size(279, 100);
            this.txtNotes.TabIndex = 10;
            this.txtNotes.TextChanged += new System.EventHandler(this.txtNotes_TextChanged);
            this.txtNotes.Enter += new System.EventHandler(this.txtNotes_Enter);
            this.txtNotes.Leave += new System.EventHandler(this.txtNotes_Leave);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(222, 201);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(35, 13);
            this.label10.TabIndex = 41;
            this.label10.Text = "Notes";
            // 
            // frmPresEntry
            // 
            this.AcceptButton = this.btnSave;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(537, 468);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.txtNotes);
            this.Controls.Add(this.lblRecNo);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.pnlControls);
            this.Controls.Add(this.picLogin);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.lblLoginName);
            this.Controls.Add(this.pnlDates);
            this.Controls.Add(this.grpSamples);
            this.Controls.Add(this.pnlPolls);
            this.Name = "frmPresEntry";
            this.Load += new System.EventHandler(this.frmPresEntry_Load);
            this.pnlPolls.ResumeLayout(false);
            this.pnlPolls.PerformLayout();
            this.grpSamples.ResumeLayout(false);
            this.grpSamples.PerformLayout();
            this.pnlDates.ResumeLayout(false);
            this.pnlDates.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picLogin)).EndInit();
            this.pnlControls.ResumeLayout(false);
            this.pnlControls.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel pnlPolls;
        private System.Windows.Forms.DateTimePicker dtimEnd;
        private System.Windows.Forms.DateTimePicker dtimBegin;
        private System.Windows.Forms.ComboBox cmbTerm;
        private System.Windows.Forms.ComboBox cmbPres;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmbPollOrg;
        private System.Windows.Forms.GroupBox grpSamples;
        private System.Windows.Forms.TextBox txtNoOpin;
        private System.Windows.Forms.TextBox txtDisappove;
        private System.Windows.Forms.TextBox txtApprove;
        private System.Windows.Forms.TextBox txtSample;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel pnlDates;
        private System.Windows.Forms.Label lblLastUser;
        private System.Windows.Forms.Label lblLastUpdt;
        private System.Windows.Forms.Label lblCreatedOn;
        private System.Windows.Forms.Label lblLoginName;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.PictureBox picLogin;
        private System.Windows.Forms.Panel pnlControls;
        private System.Windows.Forms.Button btnLast;
        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.TextBox txtGoTo;
        private System.Windows.Forms.Button btnPrev;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnGoto;
        private System.Windows.Forms.Button btnFirst;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button btnNew;
        private System.Windows.Forms.Label lblRecNo;
        private System.Windows.Forms.TextBox txtNotes;
        private System.Windows.Forms.Label label10;
    }
}

