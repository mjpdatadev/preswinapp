using System;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Configuration;

namespace PresEntry
{
    public class AppConn
    {
        
        public AppConn()
        {
        }

        public static string MakeConnection()
        {

            string connstring = "";
            try
            {
                ReUse ipGet = new ReUse();
                connstring = ipGet.GetLocalConnString(); 
              //  HttpContext.Current.Session["connVar"] = ipGet.GetLocalConnString();  //"Server=MARTINP-W530\\SQLMPECK14;Initial Catalog=GranPOS;User ID=MPAdmin; Password=XSW23edc;";
                if (String.IsNullOrEmpty(connstring)) { connstring = " "; }

            }
            catch (Exception e)
            {
              //  ReUse getConnStr = new ReUse();
              //  connstring = getConnStr.GetConnectionString();
            }

            if (connstring.Trim().Length < 1)
            {
               // ReUse getConnStr = new ReUse();
              //  connstring = getConnStr.GetConnectionString();
            }
            return connstring;
        }

        public static OleDbConnection MakeOleConn()
        {
            string connstring = "";
            OleDbConnection cnnApplication = new OleDbConnection(connstring);
            cnnApplication.Open();
            return cnnApplication;

        }
        public static SqlConnection MakeSQLConn()
        {
            SqlConnection cnnApplication = new SqlConnection(AppConn.MakeConnection());
            cnnApplication.Open();
            return cnnApplication;
        }
        public static SqlConnection MakeSQLConn(string strConnString)
        {
            if (strConnString.Trim().Length < 1)
            {
                XMLConfig xmlGetSetting = new XMLConfig();
                strConnString = xmlGetSetting.GetSetting("ConnString");
                if (strConnString.IndexOf("[^^^]") > -1)    //-<========  [^^^] means that it is an encrypted XML setting so it must be decrypted.
                {
                    EncDecryp tempEncrpt = new EncDecryp();
                    strConnString = strConnString.Replace("[^^^]", "");
                    strConnString = tempEncrpt.Decrypt(strConnString);
                }
            
            }

            string connstring = strConnString;
            if (String.IsNullOrEmpty(strConnString)) { strConnString = AppConn.MakeConnection(); }
            else {
            if (strConnString.Trim().Length < 10) { strConnString = AppConn.MakeConnection(); }
            }
            SqlConnection cnnApplication = new SqlConnection(strConnString);
            cnnApplication.Open();
            return cnnApplication;
        }
        public static SqlConnection MakeSQLConn(bool Asynch)
        {
            string connstring = "";
            SqlConnection cnnApplication = new SqlConnection(AppConn.MakeConnection());
            cnnApplication.Open();
            return cnnApplication;
        }
    }
}
