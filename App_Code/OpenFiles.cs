﻿using System;
using System.Collections.Generic;
using System.Web;
using System.IO;
using System.Xml;

/// <summary>
/// Summary description for OpenFiles
/// </summary>
public class OpenFiles
{
	public OpenFiles()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    //-------------- Begin File Path ---------------
    private string filPath;
    public string FilesPath
    {

        get { return filPath; }

        set { filPath = value; }
    }
    //-------------- End  File Path ---------------
    //-------------- Begin FileType ---------------
    private string filType;    //<--------------------- valid FileTpyes are XML or TXT
    public string FileType
    {

        get { return filType; }

        set { filType = value; }
    }

    //-------------- End  FileType ---------------
    //-------------- Begin FieldName ---------------
    private string fldName;   //<----------------------- FieldName is used with XML files
    public string FieldName
    {

        get { return fldName; }

        set { fldName = value; }
    }
    //-------------- End  FieldName ---------------
    public string GetValue()
    {

        string retValue = "";

        //-------  This return one value from an XML file ----------------- 
        if ((this.FileType == "XML") && FieldName.IndexOf("All") < 0)  //<------ if this is XML with a field name then get it.
        {
            XmlTextReader xmlReadr = new XmlTextReader(FilesPath);

            while (xmlReadr.Read())
            {
               // xmlReadr.ReadStartElement(FieldName);

                if (xmlReadr.NodeType == XmlNodeType.Element)
                {
                    if (xmlReadr.Name == FieldName)
                    {
                        retValue = xmlReadr.ReadElementContentAsString();
                        if (!String.IsNullOrEmpty(retValue)) { break;  }
                    }
                }
            }
            try
            {
                xmlReadr.Close();
                xmlReadr = null;
            }
            catch
            {
            }

        }
        else         //<---------------------------------------------- else if this is a TXT file return the whole thing.
        {
            using (StreamReader sr = new StreamReader(FilesPath))
            {
                String line = sr.ReadToEnd();
                retValue = line;
                //<-------------- This is probably a text file so get the one value between <  >  
                if (FieldName.IndexOf("All") < 0)
                {
                    int ltLoc = 0;
                    int gtLoc = 0;
                    ltLoc = retValue.IndexOf("<");
                    gtLoc = retValue.IndexOf(">");
                    if (ltLoc < 0 || gtLoc < 0)
                    {
                        retValue = "ERROR - no delimiters for text filed found.";
                    }
                    if (ltLoc > gtLoc)
                    {
                        retValue = "ERROR - no delimiters for text filed found.";
                    }
                    if (retValue.IndexOf("ERROR", 0, StringComparison.CurrentCulture) < 0)
                    {
                        retValue = retValue.Substring(ltLoc + 1, (gtLoc - ltLoc - 1));
                    }
                    sr.Close();
                }
            }
        }

        return retValue;
    }



}