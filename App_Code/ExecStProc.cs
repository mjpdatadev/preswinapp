using System;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.Diagnostics;
using PresEntry;


/// <summary>
/// Summary description for ExecStProc
/// </summary>
public class ExecStProc
{
   // SqlCommand sqlComm;
    SqlConnection grdConn;
    //strStProcSyntax - syntax is uspMyStoredProc(myvalue1, myvalue2, myvalue3,...etc)  or if    ----------
    //------ there are no params then just uspMyStoredProc ----------------
	public ExecStProc()
	{   
    }

    public SqlDataReader CreateSpReader(string strStProcSyntax, string strConn = "noConn")
    {
        grdConn = AppConn.MakeSQLConn(strConn);

        string uspStProc = strStProcSyntax;  //<---- stored proc name
        string uspStProcParms = strStProcSyntax; //<---- stored proc parameters
        int leftParen = uspStProcParms.IndexOf("(", 0);
        if (leftParen > 0)
        {
            uspStProc = uspStProc.Substring(0, leftParen);
            //----- clean the sp name ------
            uspStProc = uspStProc.Replace("(", "");
            uspStProc = uspStProc.Replace(")", "");
            uspStProcParms = uspStProcParms.Substring(uspStProcParms.IndexOf("(", 0), (uspStProcParms.Length - uspStProcParms.IndexOf("(", 0)));
        }
        else
        {
            uspStProc = strStProcSyntax;
            uspStProcParms = "";
        }
        string[] aryParams = null;
        uspStProc = uspStProc.Replace("'", "");

        SqlCommand sqlComm = new SqlCommand(uspStProc);
        sqlComm.Connection = grdConn;
        sqlComm.CommandType = CommandType.StoredProcedure;
        sqlComm.Parameters.Clear();

        if (uspStProcParms.Length > 2)
        {
            //----- clean the params and put them into an array ------
            uspStProcParms = uspStProcParms.Replace("(", "");
            uspStProcParms = uspStProcParms.Replace(")", "");
            //---- if num of params only = 1 then prep it ------
            if (uspStProcParms.Length > 0 && uspStProcParms.IndexOf(",", 0) < 1) { uspStProcParms = uspStProcParms + ", "; }
            //---- split the params ------
            aryParams = uspStProcParms.Split(new char[] { ',' });


            string dbName = grdConn.Database.ToString();
            //---- get the schema of the connection for this particular storproc -------
            // string-array layout = { DbName,Owner,SpName,Parameter } note: nulls return everything. ----
            string[] spParams = { dbName, null, uspStProc, null };
            //--- fill a DataTable  ----
            DataTable spDtTable = grdConn.GetSchema("ProcedureParameters", spParams);
            //--- from here you just filter the table ---------------
            int rws = 0;
            rws = spDtTable.Rows.Count;
            int colToGet = 0;
            int parmModeloc = 0;
            colToGet = spDtTable.Columns["PARAMETER_NAME"].Ordinal;
            parmModeloc = spDtTable.Columns["PARAMETER_MODE"].Ordinal;
            for (int rowvals = 0; rowvals < rws; rowvals++)
            {
                string srchStr = "ORDINAL_POSITION = " + (rowvals+1).ToString();
                DataRow[] rowValus= spDtTable.Select(srchStr, "ORDINAL_POSITION ASC");
               SqlParameter spParam = new SqlParameter();
               spParam.ParameterName = rowValus[0].ItemArray[colToGet].ToString();
                if (spDtTable.Rows[rowvals][8].ToString().IndexOf("char") > -1)
                {
                    spParam.Size = Convert.ToInt32(spDtTable.Rows[rowvals][9].ToString());
                }
                if (rowvals <= aryParams.GetUpperBound(0))
                {
                    sqlComm.Parameters.AddWithValue(spParam.ParameterName.ToString(), aryParams[rowvals]);
                }
                else
                {
                    sqlComm.Parameters.AddWithValue(spParam.ParameterName.ToString(), "");
                }
                if (rowValus[0].ItemArray[parmModeloc].ToString() == "IN")
                { sqlComm.Parameters[spParam.ParameterName.ToString()].Direction = ParameterDirection.Input; }
                else
                {
                   // sqlComm.Parameters[spParam.Name.ToString()].SqlDbType = SqlDbType.;
                    sqlComm.Parameters[spParam.ParameterName.ToString()].Direction = ParameterDirection.InputOutput;
                    sqlComm.Parameters[spParam.ParameterName.ToString()].Value = "1";
                    sqlComm.Parameters[spParam.ParameterName.ToString()].IsNullable = true;
                }
            }
        }
        
        SqlDataReader sqlReader = sqlComm.ExecuteReader();
        
        return sqlReader;
	}
}
