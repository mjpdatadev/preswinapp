﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Configuration;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Text;


/// <summary>
/// Summary description for ReUse
/// </summary>
public class ReUse
{
	public ReUse()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    #region  //======================================================  Begin GetConnectionString ===================================================
    public string GetConnectionString()
    {
        string returnStr = "";

        string cnString = "";

        if (cnString.IndexOf("SERVERNAME") > 0)
        {
            string sServerName = "";
            string filServerFile = "c:\\junk\\serverNm.txt";

            OpenFiles filToOpen = new OpenFiles();
            filToOpen.FileType = "XML";
            filToOpen.FilesPath = filServerFile;
            filToOpen.FieldName = "FeeldTwo";
            sServerName = filToOpen.GetValue();
            cnString = cnString.Replace("SERVERNAME", sServerName);
            returnStr = cnString;
        }
        return returnStr;
    }
    #endregion //==================================================  ***END*** GetConnectionString ===================================================

    #region  //======================================================  Begin GetLocalConnString ===================================================
    public string GetLocalConnString()
    {
        string returnStr = "";
        string currTCPIP = "10.17.8.1";

        string cnString = "";

        if (cnString.IndexOf("SERVERNAME") > 0)
        {
            cnString = cnString.Replace("SERVERNAME",currTCPIP);
        }
        returnStr = cnString;
        return returnStr;
    }
    #endregion //==================================================  ***END*** GetLocalConnString ===================================================

    #region  //======================================================  Begin IsValidEmail ===================================================
    public bool IsValidEmail(string strIn)
    {
        bool invalid = false;
        if (String.IsNullOrEmpty(strIn))
            return false;

        // Use IdnMapping class to convert Unicode domain names.
        try
        {
            strIn = Regex.Replace(strIn, @"(@)(.+)$", this.DomainMapper,
                                  RegexOptions.None);
        }
        catch (Exception ex)
        {
            return false;
        }

        if (invalid)
            return false;

        // Return true if strIn is in valid e-mail format.
        try
        {
            return Regex.IsMatch(strIn,
                  @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
                  @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$",
                  RegexOptions.IgnoreCase);
        }
        catch (Exception regexc)
        {
            return false;
        }
    }
    #endregion //==================================================  ***END*** IsValidEmail ===================================================

    #region  //======================================================  Begin DomainMapper ===================================================
    private string DomainMapper(Match match)
    {
        /*  THIS IS PART OF VALIDATING A CORRECT EMAIL ADDRESS (SEE ABOVE)   >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> */

        // IdnMapping class with default property values.
        IdnMapping idn = new IdnMapping();

        string domainName = match.Groups[2].Value;
        try
        {
            domainName = idn.GetAscii(domainName);
        }
        catch (ArgumentException)
        {
            return "false";
        }
        return match.Groups[1].Value + domainName;
    }
    #endregion //==================================================  ***END*** DomainMapper ===================================================

    #region  //======================================================  Begin NewPasswrd ====================================================
    internal string[]  NewPasswrd()
    {
        string[] strNewPassword;
        strNewPassword = new string[2];
        DateTime currTime = DateTime.Now;
        string strDatTim = String.Empty;
        string oneChar = String.Empty;
        bool isInt = false;
        int oneInt = 0;
        int randmInt = 0;
        string strFullStr = String.Empty;

        try
        {
            strDatTim = currTime.ToString().Trim();
            Random randNO = new Random();
            char oneLetter;

            while (strDatTim.Trim().Length > 0)
            {
                oneChar = strDatTim.Substring(0, 1);
                if (oneChar.Trim().Length > 0)
                {
                    isInt = Int32.TryParse(oneChar, out oneInt);
                    if (isInt == true)
                    {
                    TryAgain:
                        if (oneInt < 1) { oneInt = 1; }
                        if (oneInt > 1000) { oneInt = oneInt/100; }
                        oneInt = oneInt * 15;
                        oneInt = randNO.Next(0, oneInt);
                        if (oneInt < 35 || oneInt > 99) { goto TryAgain; }
                        if (oneInt.ToString().Trim().Length > 1)
                        {
                            oneChar = oneInt.ToString().Trim();
                            oneChar = oneChar.Substring(0, 2);
                            oneInt = Int32.Parse(oneChar);
                            if (oneInt > 34 && oneInt < 100)
                            {
                                oneChar = ((char)oneInt).ToString();
                            }
                            //      oneLetter = (char)oneInt;
                            //      oneChar = oneLetter.ToString();
                        }
                        else
                        {
                            oneChar = oneInt.ToString().Trim();
                        }
                        strFullStr = strFullStr + oneChar.Trim();
                    }
                }
                strDatTim = strDatTim.Substring(1, strDatTim.Trim().Length - 1);
            }
            strFullStr = strFullStr.Replace("\\", "");
            strNewPassword[0] = strFullStr.Substring(0, 8);
            strNewPassword[0] = strNewPassword[0].Replace(@"\", "");
            strNewPassword[0] = strNewPassword[0].Replace(@"00", "");

            EncDecryp newPass = new EncDecryp();
            strNewPassword[1] = newPass.Encrypt(strNewPassword[0]);

        }
        catch (Exception exValid)
        {
            MessageBox.Show("This is my error: " + exValid.Message, "Error Found in NewPasswrd", MessageBoxButtons.OK);
        }
        return strNewPassword;
    }
    #endregion //==================================================  ***END*** NewPasswrd ==================================================



}