﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using System.Windows.Forms;

namespace PresEntry
{
    class XMLConfig
    {
        /// <summary>
        /// These are the settings for each individual application. Please add this class to your project and then modify these to suit.
        /// </summary>
        //--------- This is the path to your settings file. Hard code it or add NKSettings to your project and use NKFolders. See the constructor. ------
        static string SettingsPath = "";
        //---------  This is the name that you want to give to your file. Examples: MySettings.xml, MySettings.cfg, POSEnt.xml, MenuPlan.cfg
        static string CFGFileName = "PresRate.xml";
        //---------  If the file does not exist it will be created. This property will be the name of the root node of your XML.
        static string ForApplication = "PresRate";

        public XMLConfig()
        {
            SettingsPath = Path.GetDirectoryName(Application.ExecutablePath);
            SettingsPath = PathEndsInSlash(SettingsPath, true);
            //   SettingsPath = "C:\\ProgramData"; <----- if the path to the settings folder is not in NKFolders then modify it here. -----------

            if (ForApplication.Trim().Length < 1)
            {
                throw new ArgumentException("You must specify the application (property) that that these settings apply to.");
            }
            if (SettingsPath.Trim().Length < 1)
            {
                throw new ArgumentException("You must specify the where to store the settings file (property) before using it.");
            }
            if (CFGFileName.Trim().Length < 1)
            {
                throw new ArgumentException("You must specify the name of the settings file (property) before using it.");
            }

            if (SettingsPath.Trim().Length > 2)
           {
                if (!File.Exists(SettingsPath + CFGFileName))
                {
                    CreateBaseSettingsFile();
                }
            }
        }
    

    private bool CreateBaseSettingsFile()
    {
        bool fileCreated = false;

        string strBaseXML = @"<?xml version='1.0' encoding='utf-8'?>";
        string strRootTop = "";
        string strRootEnd = "";
        strRootTop = "<" + ForApplication + ">";
        strRootEnd = "</" + ForApplication + ">";

        try
        {
            System.IO.File.WriteAllText(SettingsPath + CFGFileName, strBaseXML+strRootTop+strRootEnd );
            fileCreated = true;
        }
        catch
        {
            throw new ArgumentException("Cannot create the setting file.");
        }
        return fileCreated;
    }
//======================================================================= Save ==============================================
    public bool SaveSetting(string nodeName, string nodeValue)
    {
        bool didSave = false;
 
        try
        {
            XDocument xdocSetting = XDocument.Load(SettingsPath + CFGFileName);
            //tempReader.Close();
            XName topNode = XName.Get(ForApplication);
            XName saveNode = XName.Get(nodeName);

            if (xdocSetting.Descendants(nodeName).Count()>0)
            {
                xdocSetting.Descendants(nodeName).ElementAt(0).SetValue(nodeValue);
            }
            else
            {
                XElement newElem = new XElement(nodeName, nodeValue);
                xdocSetting.Element(topNode).Add(newElem);
            }

            xdocSetting.Save(SettingsPath + CFGFileName);

        }
        catch (Exception savEx)
        {
            throw new ArgumentException("There was a problem trying to find the Settings node called " + nodeName);
        }
        return didSave;
    
    }
    //======================================================================= Get ==============================================
    public string GetSetting(string nodeName)
    {
            string[] aryNodes;
            aryNodes = nodeName.Split(':');
            string SettingVal = "";
        try
        {
            XDocument xdocSetting = XDocument.Load(SettingsPath + CFGFileName);
                //XName getNode = XName.Get(nodeName);

                switch (aryNodes.Length)
                {
                    case 2:
                        SettingVal = xdocSetting.Descendants(aryNodes[0]).Descendants(aryNodes[1]).ElementAt(0).Value;
                        break;
                    case 3:
                        SettingVal = xdocSetting.Descendants(aryNodes[0]).Descendants(aryNodes[1]).Descendants(aryNodes[2]).ElementAt(0).Value;
                        break;
                    default:
                        SettingVal = xdocSetting.Descendants(aryNodes[0]).ElementAt(0).Value;
                        break;
                }

            //    SettingVal = xdocSetting.Descendants(nodeName).ElementAt(0).Value;

                if (SettingVal.IndexOf("[^^^]") > -1)    //-<========  [^^^] means that it is an encrypted XML setting so it must be decrypted.
                {
                    EncDecryp tempEncrpt = new EncDecryp();
                    SettingVal = SettingVal.Replace("[^^^]", "");
                    SettingVal = tempEncrpt.Decrypt(SettingVal);
                }

            }
        catch (Exception getEx)
        {
            SettingVal = "";
            if (getEx.Message.IndexOf("out of the range of valid values", StringComparison.CurrentCultureIgnoreCase) > 0)
            {
                SaveSetting(nodeName, "");
                return SettingVal;
            }
            throw new ArgumentException("There was a problem trying to find the XML node called " + nodeName);
        }
        return SettingVal;
    }

    //======================================================================= PathEndsInSlash ==============================================

    public string PathEndsInSlash(string strPathWOfilename, bool bSlashReqd)
    {
        string FixedPath = String.Empty;

        if (bSlashReqd)
        {
            if (strPathWOfilename.Substring((strPathWOfilename.Length - 1), 1) == "\\")
            {
                FixedPath = strPathWOfilename;
            }
            else
            {
                FixedPath = strPathWOfilename + "\\";                
            }
        }
        else  //--------------vvvvvv------ Remove the Slash.
        {
            if (strPathWOfilename.Substring((strPathWOfilename.Length - 1), 1) == "\\")
            {
                FixedPath = strPathWOfilename.Substring(0,(strPathWOfilename.Length - 1));
            }
            else
            {
                FixedPath = strPathWOfilename;
            }
        }
        return FixedPath;
    }
}



}
