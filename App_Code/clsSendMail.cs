﻿using System;
using System.Net;
using System.Net.Mail;

namespace PresEntry
{
    class clsSendMail
    {
        MailMessage mail;
        public clsSendMail(string FromAddr, string ToAddr)
        {
            mail = new MailMessage(FromAddr,ToAddr);
        }
#region  //======================================================  Begin SendEmail ====================================================
        internal bool SendEmail(string strSubjectLine, string strMyMessage)
        {
            bool isSent = false;
            string bouncerAddr = "";
            string bouncerPWD = "";

            try
            {
                SmtpClient client = new SmtpClient();
                client.Port = Int32.Parse(EmailGetSettings("EmailSettings:EmailPort")); //465; //587
                client.EnableSsl = true;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.UseDefaultCredentials = false;
                bouncerAddr = EmailGetSettings("EmailSettings:FromAddr");  //--<-- encrypted
                bouncerPWD = EmailGetSettings("EmailSettings:FromPWD");  //--<-- encrypted
                client.Host = EmailGetSettings("EmailSettings:ClientHost");
                client.Credentials = new NetworkCredential(bouncerAddr, bouncerPWD);
                mail.Subject = strSubjectLine;
                mail.Body = strMyMessage;
                client.Send(mail);
            }
            catch (Exception exMail)
            {
                isSent = false;
            }

            return isSent;
        }
        #endregion //==================================================  ***END*** SendEmail ====================================================

        #region  //======================================================  Begin EmailGetSettings ====================================================
        internal string EmailGetSettings(string xmlEMailNode)
        {
            string returnValu = "";

            XMLConfig xmlGetSetting = new XMLConfig();
            returnValu = xmlGetSetting.GetSetting(xmlEMailNode);

            return returnValu;
        }
        #endregion //==================================================  ***END*** EmailGetSettings ====================================================


    }
}
