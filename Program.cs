﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace PresEntry
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            
            //==================== use this to encrypt strings ===================
            //Server=DEVSRVMP1;Database=PresRelate;User Id=superuser;Password=ZAQ12wsx;
        /*    string strWhatToEncrypt = @"Server=DEVSRVMP1;Database=PresRelate;User Id=superuser;Password=ZAQ12wsx;";
            string strEncrypted = "";
            EncDecryp tempEncrpt = new EncDecryp();
            strEncrypted = tempEncrpt.Encrypt(strWhatToEncrypt);
            strWhatToEncrypt = tempEncrpt.Decrypt(strEncrypted);
            int stopper = 0;*/
            //====================================================================
           
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new frmPresEntry());

        }
    }
}
