﻿namespace PresEntry
{
    partial class frmNewUser
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtUsrFName = new System.Windows.Forms.TextBox();
            this.txtUsrLName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtEmailAddr = new System.Windows.Forms.TextBox();
            this.txtPasswd1 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btnUserSave = new System.Windows.Forms.Button();
            this.btnUserCancel = new System.Windows.Forms.Button();
            this.chkMakeUsers = new System.Windows.Forms.CheckBox();
            this.chkChngPWD = new System.Windows.Forms.CheckBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtPWDConfirm = new System.Windows.Forms.TextBox();
            this.chkShow = new System.Windows.Forms.CheckBox();
            this.btnSendMail = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(39, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "First Name:";
            // 
            // txtUsrFName
            // 
            this.txtUsrFName.Location = new System.Drawing.Point(107, 22);
            this.txtUsrFName.Name = "txtUsrFName";
            this.txtUsrFName.Size = new System.Drawing.Size(156, 20);
            this.txtUsrFName.TabIndex = 1;
            // 
            // txtUsrLName
            // 
            this.txtUsrLName.Location = new System.Drawing.Point(107, 49);
            this.txtUsrLName.Name = "txtUsrLName";
            this.txtUsrLName.Size = new System.Drawing.Size(156, 20);
            this.txtUsrLName.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(37, 53);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(61, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Last Name:";
            // 
            // txtEmailAddr
            // 
            this.txtEmailAddr.Location = new System.Drawing.Point(107, 77);
            this.txtEmailAddr.Name = "txtEmailAddr";
            this.txtEmailAddr.Size = new System.Drawing.Size(156, 20);
            this.txtEmailAddr.TabIndex = 4;
            // 
            // txtPasswd1
            // 
            this.txtPasswd1.Location = new System.Drawing.Point(107, 105);
            this.txtPasswd1.Name = "txtPasswd1";
            this.txtPasswd1.PasswordChar = '*';
            this.txtPasswd1.Size = new System.Drawing.Size(97, 20);
            this.txtPasswd1.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(23, 80);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(76, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Email Address:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(42, 108);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Password:";
            // 
            // btnUserSave
            // 
            this.btnUserSave.Location = new System.Drawing.Point(61, 210);
            this.btnUserSave.Name = "btnUserSave";
            this.btnUserSave.Size = new System.Drawing.Size(75, 23);
            this.btnUserSave.TabIndex = 8;
            this.btnUserSave.Text = "Save";
            this.btnUserSave.UseVisualStyleBackColor = true;
            this.btnUserSave.Click += new System.EventHandler(this.btnUserSave_Click);
            // 
            // btnUserCancel
            // 
            this.btnUserCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnUserCancel.Location = new System.Drawing.Point(142, 210);
            this.btnUserCancel.Name = "btnUserCancel";
            this.btnUserCancel.Size = new System.Drawing.Size(75, 23);
            this.btnUserCancel.TabIndex = 9;
            this.btnUserCancel.Text = "Close";
            this.btnUserCancel.UseVisualStyleBackColor = true;
            // 
            // chkMakeUsers
            // 
            this.chkMakeUsers.AutoSize = true;
            this.chkMakeUsers.Location = new System.Drawing.Point(93, 164);
            this.chkMakeUsers.Name = "chkMakeUsers";
            this.chkMakeUsers.Size = new System.Drawing.Size(134, 17);
            this.chkMakeUsers.TabIndex = 10;
            this.chkMakeUsers.Text = "Can Create New Users";
            this.chkMakeUsers.UseVisualStyleBackColor = true;
            // 
            // chkChngPWD
            // 
            this.chkChngPWD.AutoSize = true;
            this.chkChngPWD.Location = new System.Drawing.Point(93, 187);
            this.chkChngPWD.Name = "chkChngPWD";
            this.chkChngPWD.Size = new System.Drawing.Size(138, 17);
            this.chkChngPWD.TabIndex = 11;
            this.chkChngPWD.Text = "Must Change Password";
            this.chkChngPWD.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 137);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(94, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "Confirm Password:";
            // 
            // txtPWDConfirm
            // 
            this.txtPWDConfirm.Location = new System.Drawing.Point(107, 134);
            this.txtPWDConfirm.Name = "txtPWDConfirm";
            this.txtPWDConfirm.PasswordChar = '*';
            this.txtPWDConfirm.Size = new System.Drawing.Size(97, 20);
            this.txtPWDConfirm.TabIndex = 13;
            // 
            // chkShow
            // 
            this.chkShow.AutoSize = true;
            this.chkShow.Checked = true;
            this.chkShow.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkShow.Location = new System.Drawing.Point(210, 107);
            this.chkShow.Name = "chkShow";
            this.chkShow.Size = new System.Drawing.Size(53, 17);
            this.chkShow.TabIndex = 14;
            this.chkShow.Text = "Show";
            this.chkShow.UseVisualStyleBackColor = true;
            this.chkShow.CheckedChanged += new System.EventHandler(this.chkShow_CheckedChanged);
            // 
            // btnSendMail
            // 
            this.btnSendMail.Location = new System.Drawing.Point(102, 239);
            this.btnSendMail.Name = "btnSendMail";
            this.btnSendMail.Size = new System.Drawing.Size(75, 23);
            this.btnSendMail.TabIndex = 15;
            this.btnSendMail.Text = "Send Email";
            this.btnSendMail.UseVisualStyleBackColor = true;
            this.btnSendMail.Visible = false;
            this.btnSendMail.Click += new System.EventHandler(this.btnSendMail_Click);
            // 
            // frmNewUser
            // 
            this.AcceptButton = this.btnUserSave;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnUserCancel;
            this.ClientSize = new System.Drawing.Size(284, 273);
            this.ControlBox = false;
            this.Controls.Add(this.btnSendMail);
            this.Controls.Add(this.chkShow);
            this.Controls.Add(this.txtPWDConfirm);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.chkChngPWD);
            this.Controls.Add(this.chkMakeUsers);
            this.Controls.Add(this.btnUserCancel);
            this.Controls.Add(this.btnUserSave);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtPasswd1);
            this.Controls.Add(this.txtEmailAddr);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtUsrLName);
            this.Controls.Add(this.txtUsrFName);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "frmNewUser";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Add New User";
            this.Load += new System.EventHandler(this.frmNewUser_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtUsrFName;
        private System.Windows.Forms.TextBox txtUsrLName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtEmailAddr;
        private System.Windows.Forms.TextBox txtPasswd1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnUserSave;
        private System.Windows.Forms.Button btnUserCancel;
        private System.Windows.Forms.CheckBox chkMakeUsers;
        private System.Windows.Forms.CheckBox chkChngPWD;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtPWDConfirm;
        private System.Windows.Forms.CheckBox chkShow;
        private System.Windows.Forms.Button btnSendMail;
    }
}