﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Windows.Forms;

namespace PresEntry
{

    public partial class frmLogin : Form
    {
        #region //===============================  Property  UserNo  ====================================
        private int user_num;
        internal int UserNo
        {
            get
            {
                return user_num;
            }
            set
            {
                user_num = value;
            }
        }
        #endregion  //===============================  End Property UserNo  ================================
        #region //===============================  Property  AdminUser  ====================================
        private int is_admin;
        internal int AdminUser
        {
            get
            {
                return is_admin;
            }
            set
            {
                is_admin = value;
            }
        }
        #endregion  //===============================  End Property AdminUser  ================================

        public frmLogin()
        {
            InitializeComponent();
        }

        private void btnLogExit_Click(object sender, EventArgs e)
        {
            this.Close();
            this.Dispose();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            Application.DoEvents();
            string strPass = String.Empty;
            strPass = txtPasswd.Text;

            //---------------- for testing REMOVE ------------------------
      //      ReUse passTest = new ReUse();
     //       string[] strTestNew = passTest.NewPasswrd();

            //------------------------------------------------------------

            EncDecryp encPass = new EncDecryp();
            strPass = encPass.Encrypt(strPass);

            ExecStProc xspLogin = new ExecStProc();
            SqlDataReader rdrLogin = xspLogin.CreateSpReader("uspLoginUser(" + txtEmailAddr.Text + "," + strPass + ")", "");

            if (rdrLogin.HasRows)
            {
                rdrLogin.Read();
                if (rdrLogin[1].ToString().IndexOf("error", StringComparison.CurrentCultureIgnoreCase) > -1)
                {
                    lblLogMsg.Text = "Invalid User Name or Password";
                    lblLogMsg.ForeColor = Color.Red;
                    txtPasswd.Text = "";
                }
                else 
                {
                    int iUserID = 0;
                    int iMustReset = 0;
                    bool isNumber = false;
                    bool resetPWD = false;
                    resetPWD = Int32.TryParse(rdrLogin[3].ToString(), out iMustReset);
                    isNumber = Int32.TryParse(rdrLogin[0].ToString(),out iUserID);
                    if (isNumber && iUserID > 0)
                    {
                        this.UserNo = iUserID;
                        if (resetPWD && iMustReset == 1)
                        {
                            MessageBox.Show("You must change your password before you continue.", "Change Required.", MessageBoxButtons.OK);
                            btnChangePWD.Enabled = true;
                            btnChangePWD.Visible = true;
                            btnChangePWD.PerformClick();
                        }
                        else
                        {
                            Form thisOwner = new Form();
                            Label ownedCntrl = new Label();
                            thisOwner = (Form)this.Owner;
                            ownedCntrl = (Label)thisOwner.Controls.Find("lblLoginName", true)[0];
                            ownedCntrl.Text = "Currently Logged in as: " + rdrLogin[1].ToString() + " " + rdrLogin[2].ToString();
                            ownedCntrl.Tag = rdrLogin[0].ToString(); //-<----- tag of the lblLoginName holds their user id.
                            thisOwner.Tag = rdrLogin[4].ToString();  //-<----- tag of the main form shows if they are an Admin.
                            this.Close();
                            this.Dispose();
                        }
                    }
                    else
                    {
                        lblLogMsg.Text = "Invalid User Name or Password";
                        lblLogMsg.ForeColor = Color.Red;
                        txtPasswd.Text = "";
                    }
                }
            }
            Cursor.Current = Cursors.Default;

        }
        #region  //======================================================  Begin btnAddNew_Click ================================================================
        private void btnAddNew_Click(object sender, EventArgs e)
        {
            frmNewUser fmNewUsr = new frmNewUser();
            fmNewUsr.UsrNumber = this.UserNo;
            fmNewUsr.ForgotWhich = 0;   //--<------ zero means they forgot nothing. default.
            fmNewUsr.ShowDialog(this);
        }
        #endregion //==================================================  ***END*** btnAddNew_Click ================================================================

        #region  //======================================================  Begin frmLogin_Load ================================================================
        private void frmLogin_Load(object sender, EventArgs e)
        {
#if Debug
            txtEmailAddr.Text="mart01@mjpdata.com"; 
#else
            txtEmailAddr.Text = "mart01@mjpdata.com";
#endif
            if (this.AdminUser == 1)
            { btnAddNew.Visible = true; }
            else
            { btnAddNew.Visible = false; }
            if (this.UserNo > 0)
            { btnChangePWD.Visible = true; }
            else
            { btnChangePWD.Visible = false; }
        }
        #endregion //==================================================  ***END*** frmLogin_Load ================================================================

        private void bntForgotUsrNm_Click(object sender, EventArgs e)
        {
            frmNewUser fmForgotUsr = new frmNewUser();
            fmForgotUsr.ForgotWhich = 1;   //--<------ one means they forgot their username.
            fmForgotUsr.ShowDialog(this);
        }

        private void btnForgotPWD_Click(object sender, EventArgs e)
        {
            frmNewUser fmForgotPwd = new frmNewUser();
            fmForgotPwd.ForgotWhich = 2;   //--<------ two means they forgot their password.
            fmForgotPwd.ShowDialog(this);
        }

        private void btnChangePWD_Click(object sender, EventArgs e)
        {
            frmNewUser fmForgotPwd = new frmNewUser();
            fmForgotPwd.ForgotWhich = 3;   //--<------ three means they want to change their password.
                                           /*          TextBox txbEmailAddr = (TextBox) fmForgotPwd.Controls.Find("txtEmailAddr", true)[0];
                                                     txbEmailAddr.Text = txtEmailAddr.Text;
                                                     txbEmailAddr.Enabled = false;*/
            fmForgotPwd.UsrNumber = this.UserNo;
            fmForgotPwd.ShowDialog(this);
        }
    }
}
