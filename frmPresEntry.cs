﻿#define Debug      // Debugging on

using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace PresEntry
{
    
    public partial class frmPresEntry : Form
    {
        #region //===============================  Property  IsDirty  ====================================
        private bool isDirty;
        public bool is_dirty
        {
            get
            {
                return is_dirty;
            }
            set
            {
                is_dirty = value;
            }
        }
        #endregion  //===============================  End Property  IsDirty  ====================================
        #region //===============================  Property  IsAdmin  ====================================
        private bool isAdmin;
        public bool is_admin
        {
            get
            {
                return is_admin;
            }
            set
            {
                is_admin = value;
            }
        }
        #endregion  //=============================== End  Property  IsAdmin  ====================================

        DataTable dtTerms = new DataTable();

        public frmPresEntry()
        {
            InitializeComponent();
        }


        private void lblRecNo_TextChanged(object sender, EventArgs e)
        {
            if (Int32.Parse(lblRecNo.Text) == -1)
            {
                LoadPollDefaults();
            }
            else
            {
                LoadPollRecord(Int32.Parse(lblRecNo.Text));
            }
            this.isDirty = false;
        }

        #region //===============================  btnExit_Click  ====================================
        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
            this.Dispose();
            Application.Exit();
        }
        #endregion //=============================  End btnExit_Click  ==================================

        #region //===============================  picLogin_Click  ====================================
        private void picLogin_Click(object sender, EventArgs e)
        {
            int LoginNo = -5;
            bool isLoginNum = false;
            isLoginNum = Int32.TryParse(lblLoginName.Tag.ToString(), out LoginNo);
            if (isLoginNum==false && LoginNo<1)
            { lblLoginName.Tag = "-1"; }
            
            frmLogin fmLogin = new frmLogin();
            fmLogin.UserNo = Int32.Parse(lblLoginName.Tag.ToString());
            fmLogin.AdminUser = Int32.Parse(this.Tag.ToString());
            fmLogin.ShowDialog(this);
            if (Int32.Parse(lblLoginName.Tag.ToString())>0)
            {
                bool isDone = ProtectForm(false);
                bool isLoaded = LoadPollDefaults();
                this.isDirty = false;
            }
            else
            {
                lblLoginName.Tag = "-1";
                lblLoginName.Text = "<<<------ Click To Log In";
                this.Tag = "-2";
                bool isDone = ProtectForm(true);
            }

        }
        #endregion //=============================  End picLogin_Click  ==================================


        #region //===============================  Load Form  ----  frmPresEntry_Load  ====================================
        private void frmPresEntry_Load(object sender, EventArgs e)
        {
            this.Tag = "0";
#if Debug && testing==true
            bool isDone = ProtectForm(false);
            lblLoginName.Text = "Curr User:Testor";
            lblLoginName.Tag= "1";
            lblRecNo.Text = "-1";
#else
            bool isDone = ProtectForm(true);
#endif
        }
        #endregion //=============================== END frmPresEntry_Load  ====================================

        #region //===============================  ProtectForm(bool protectIT)  ====================================
        private bool ProtectForm(bool protectIT)
        {
            bool bswapComplete = false;
            protectIT=(!protectIT);
            try
            {
                pnlControls.Enabled = protectIT;
                pnlDates.Enabled = protectIT;
                pnlPolls.Enabled = protectIT;
                grpSamples.Enabled = protectIT;
                txtNotes.Enabled = protectIT;
                bswapComplete = true;
            }
            catch
            {
                bswapComplete = false;
            }
            return bswapComplete;
        }
        #endregion //=============================== END:  ProtectForm(bool protectIT)  ====================================

        #region //===============================  LoadPollDefaults()   ====================================
        private bool LoadPollDefaults()
        {
            bool isLoaded = false;
            ExecStProc xspLoadForm = new ExecStProc();
            SqlDataReader rdrForm = xspLoadForm.CreateSpReader("uspPollNew(" + lblLoginName.Tag.ToString() + ")", "");

            if (rdrForm.HasRows)
            {
                DataTable dtPollOrgs = new DataTable();
                DataTable dtPrezes = new DataTable();
                dtTerms.Clear();
                dtPollOrgs.Load(rdrForm);
                dtPrezes.Load(rdrForm);
                dtTerms.Load(rdrForm);

                dtPollOrgs.Rows.InsertAt(LoadComboChooser(dtPollOrgs), 0);
                cmbPollOrg.DisplayMember = "PollingOrg";
                cmbPollOrg.ValueMember = "PollOrgID";
                cmbPollOrg.DataSource = dtPollOrgs;

                dtPrezes.Rows.InsertAt(LoadComboChooser(dtPrezes), 0);
                cmbPres.DisplayMember = "PresName";
                cmbPres.ValueMember = "PresID";
                cmbPres.DataSource = dtPrezes;

                DataRow rwNew;
                object[] rowArray = new object[3];
                rowArray[0] = "-1";
                rowArray[0] = "-1";
                rowArray[2] = "(choose)";
                rwNew = dtTerms.NewRow();
                rwNew.ItemArray = rowArray;
                dtTerms.Rows.InsertAt(rwNew, 0);
                cmbTerm.DisplayMember = "TermRange";
                cmbTerm.ValueMember = "TermID";
                cmbTerm.DataSource = dtTerms;
                cmbTerm.SelectedIndex = 0;

                dtimBegin.Value  = DateTime.Now;
                dtimEnd.Value = DateTime.Now;
                txtApprove.Text = "0";
                txtDisappove.Text = "0";
                txtSample.Text = "0";
                txtNoOpin.Text = "0";
                txtNotes.Text = "";
                lblCreatedOn.Text = "Created On: " + DateTime.Now.ToString("MM/dd/yyyy");
                lblLastUpdt.Text = "Last Updated On: " + DateTime.Now.ToString("MM/dd/yyyy");
                string[] sName;
                sName = lblLoginName.Text.Split(':');
                lblLastUser.Text = "Last Updated By:" + sName[1];
            }
            return isLoaded;
        }
        #endregion //=============================== END LoadPollDefaults()  ====================================

        #region //===============================  LoadPollRecord(int recNum))   ====================================
        private bool LoadPollRecord(int recNum)
        {
            bool pkLoaded = false;
            Cursor.Current = Cursors.WaitCursor;
            Application.DoEvents();
            ExecStProc xspLoadForm = new ExecStProc();
            SqlDataReader rdrForm = xspLoadForm.CreateSpReader("uspPollGetRecord(" + lblLoginName.Tag.ToString() + "," + "0.0.0.0," + recNum.ToString()  + "," + lblRecNo.Tag.ToString() + ")", "");

            if (rdrForm.HasRows)
            {
              /*DataTable dtPollOrgs = new DataTable();
                dtPollOrgs.Load(rdrForm);*/
              
                rdrForm.Read();
                lblRecNo.TextChanged -= new EventHandler(lblRecNo_TextChanged);
                lblRecNo.Text = rdrForm[0].ToString();
                txtGoTo.Text = rdrForm[0].ToString();
                lblRecNo.TextChanged += new EventHandler(lblRecNo_TextChanged);

                cmbPollOrg.SelectedValue = rdrForm[1].ToString();
                cmbPres.SelectedIndexChanged -= cmbPres_SelectedIndexChanged;
                cmbPres.SelectedValue = rdrForm[2].ToString();
                cmbTerm.SelectedValue = rdrForm[3].ToString();
                cmbPres.SelectedIndexChanged += cmbPres_SelectedIndexChanged;


                dtimBegin.Value = Convert.ToDateTime(rdrForm[4].ToString());
                dtimEnd.Value = Convert.ToDateTime(rdrForm[5].ToString());
                txtApprove.Text = rdrForm[8].ToString();
                txtDisappove.Text = rdrForm[9].ToString();
                txtSample.Text = rdrForm[7].ToString();
                txtNoOpin.Text = rdrForm[10].ToString();
                txtNotes.Tag= rdrForm[17].ToString();
                txtNotes.Text= rdrForm[18].ToString();
                lblCreatedOn.Text = "Created On: " + DateTime.Parse( rdrForm[12].ToString()).ToString("MM/dd/yyyy");
                lblLastUpdt.Text = "Last Updated On: " + DateTime.Parse(rdrForm[13].ToString()).ToString("MM/dd/yyyy");
                lblLastUser.Text = "Last Updated By: " + rdrForm[15].ToString() + " " + rdrForm[16].ToString();
            }
            Cursor.Current = Cursors.Default;
            return pkLoaded;
        }
        #endregion //=============================== END:  LoadPollRecord(int recNum))   ====================================

        #region //=============================== LoadComboChooser  ====================================
        private DataRow LoadComboChooser(DataTable dtLoadMe)
        {
            DataRow rwNew;
            object[] rowArray = new object[2];
            rowArray[0] = "-1";
            rowArray[1] = "(choose)";
            rwNew = dtLoadMe.NewRow();
            rwNew.ItemArray = rowArray;
            return rwNew;
        }
        #endregion //=============================== END LoadComboChooser  ====================================


        private void cmbPres_SelectedIndexChanged(object sender, EventArgs e)
        {
            int iChoice = Int32.Parse(cmbPres.SelectedValue.ToString());
            int indx = 0;
            for (int trms = 1; trms < dtTerms.Rows.Count; trms++)
            {
                if (Int32.Parse(dtTerms.Rows[trms][1].ToString()) == iChoice)
                {
                    cmbTerm.SelectedIndex = Int32.Parse(dtTerms.Rows[trms][0].ToString());
                    break;
                }
            }
            this.isDirty = true;
        }

        private bool SavePoll()
        {
            bool didSave = false;
            string sParams = String.Empty;
            Cursor.Current = Cursors.WaitCursor;
            Application.DoEvents();
            try
            {
                sParams = lblRecNo.Text;
                sParams += "~" + lblLoginName.Tag.ToString();
                sParams += "~" + cmbPollOrg.SelectedValue.ToString();
                sParams += "~" + cmbPres.SelectedValue.ToString();
                sParams += "~" + cmbTerm.SelectedValue.ToString();
                sParams += "~" + dtimBegin.Value.ToString();   //<---- dates must be wrapped as string when using ExStorProc
               // sParams += "~^" + dtimBegin.Value.ToString() + "^";   //<---- dates must be wrapped as string when using ExStorProc
              //  sParams += "~^" + dtimEnd.Value.ToString() + "^";   //<---- dates must be wrapped as string when using ExStorProc
                sParams += "~" + dtimEnd.Value.ToString();   //<---- dates must be wrapped as string when using ExStorProc
                sParams += "~" + txtSample.Text;
                sParams += "~" + txtApprove.Text;
                sParams += "~" + txtDisappove.Text;
                sParams += "~" + txtNoOpin.Text;
                sParams += "~" + txtNotes.Text;

                sParams = sParams.Replace("'", "''");
                sParams = sParams.Replace("~", ",");
                sParams = sParams.Replace("^", "'");  //------- convert these last. dates must be wrapped as string when using ExStorProc
                ExecStProc xspSaveForm = new ExecStProc();
                SqlDataReader rdrSave = xspSaveForm.CreateSpReader("uspPollSave(" + sParams + ")", "");

                if (rdrSave.HasRows)
                {
                    rdrSave.Read();
                    if (rdrSave[0].ToString().IndexOf("uccess") < 0)
                    {
                        didSave = false;
                        MessageBox.Show("There was an error saving your record.\r\nPlease check your connection or contact the Administrator.", "Save Error: #" + rdrSave[1].ToString(), MessageBoxButtons.OK);
                    }
                    else
                    {
                        didSave = true;
                        MessageBox.Show("Record Saved", "Save Complete #" + rdrSave[1].ToString(), MessageBoxButtons.OK);
                        lblRecNo.TextChanged -= new EventHandler(lblRecNo_TextChanged);
                        lblRecNo.Text = rdrSave[1].ToString();
                        lblRecNo.TextChanged += new EventHandler(lblRecNo_TextChanged);
                        lblCreatedOn.Text = "Created On: " + DateTime.Now.ToString("MM/dd/yyyy");
                        lblLastUpdt.Text = "Last Updated On: " + DateTime.Now.ToString("MM/dd/yyyy");
                        string[] sName;
                        sName = lblLoginName.Text.Split(':');
                        lblLastUser.Text = "Last Updated By:" + sName[1];
                        txtGoTo.Text = rdrSave[1].ToString();
                    }
                }
                else
                {
                    didSave = false;
                }
            }
            catch(Exception savEX)
            {
                didSave = false;
                MessageBox.Show("There was an error saving your record.\r\nPlease check your connection or contact the Administrator.\r\n" + savEX.Message, "Save Error", MessageBoxButtons.OK);
            }
            Cursor.Current = Cursors.Default;

            return didSave;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (IsValidPoll())
            {
                bool bDone = SavePoll();
                if (bDone == true) { this.isDirty = false;  }
            }
        }

        #region  //============================ Valid Poll is entered  =================================================================================
        private bool IsValidPoll()
        {
            bool isValPoll = true;

            if (cmbPollOrg.Text.Trim() == "(choose)")
            {
                MessageBox.Show("You must choose a valid polling organization", "Invalid Organization", MessageBoxButtons.OK);
                cmbPollOrg.Focus();
                isValPoll = false;
                goto retInvalid;
            }

            if (cmbPres.Text.Trim() == "(choose)")
            {
                MessageBox.Show("You must choose a valid president", "Invalid President", MessageBoxButtons.OK);
                cmbPres.Focus();
                isValPoll = false;
                goto retInvalid;
            }

            if (cmbTerm.Text.Trim() == "(choose)")
            {
                MessageBox.Show("You must choose a valid term of office", "Invalid Term", MessageBoxButtons.OK);
                cmbTerm.Focus();
                isValPoll = false;
                goto retInvalid;
            }

            if (dtimBegin.Value > dtimEnd.Value)
            {
                MessageBox.Show("Your poll Ending date cannot be less then your poll Begining date.", "Bad Date Range", MessageBoxButtons.OK);
                dtimEnd.Focus();
                isValPoll = false;
                goto retInvalid;            
            }

            if (txtApprove.Text.Trim().Length < 1) { txtApprove.Text = "0"; }
            if (txtDisappove.Text.Trim().Length < 1) { txtDisappove.Text = "0"; }
            if (txtSample.Text.Trim().Length < 1) { txtSample.Text = "0"; }
            if (txtNoOpin.Text.Trim().Length < 1) { txtNoOpin.Text = "0"; }
            int isNum=0;
            if (!Int32.TryParse(txtSample.Text, out isNum))
            {
                MessageBox.Show("The sample size must be numeric", "Sample not Numeric", MessageBoxButtons.OK);
                txtSample.Focus();
                txtSample.SelectAll();
                isValPoll = false;
                goto retInvalid;            
            }
            if (!Int32.TryParse(txtApprove.Text, out isNum))
            {
                MessageBox.Show("The approval value must be numeric", "Sample not Numeric", MessageBoxButtons.OK);
                txtApprove.Focus();
                txtApprove.SelectAll();
                isValPoll = false;
                goto retInvalid;
            }
            if (!Int32.TryParse(txtDisappove.Text, out isNum))
            {
                MessageBox.Show("The disapproval value must be numeric", "Sample not Numeric", MessageBoxButtons.OK);
                txtDisappove.Focus();
                txtDisappove.SelectAll();
                isValPoll = false;
                goto retInvalid;
            }
            if (!Int32.TryParse(txtNoOpin.Text, out isNum))
            {
                MessageBox.Show("The no opinion value must be numeric", "Sample not Numeric", MessageBoxButtons.OK);
                txtNoOpin.Focus();
                txtNoOpin.SelectAll();
                isValPoll = false;
                goto retInvalid;
            }
retInvalid:
            return isValPoll;
        }
        #endregion  //============================ Valid Poll is entered  =================================================================================


        #region  //============================ Textboxes cannot be blank  =================================================================================
        private void txtSample_Leave(object sender, EventArgs e)
        {
            if (txtSample.Text.Trim().Length < 1) { txtSample.Text = "0";}
            this.isDirty = true;
        }
        private void txtApprove_Leave(object sender, EventArgs e)
        {
            if (txtApprove.Text.Trim().Length < 1) { txtSample.Text = "0"; }
            this.isDirty = true;
        }
        private void txtDisappove_Leave(object sender, EventArgs e)
        {
            if (txtDisappove.Text.Trim().Length < 1) { txtSample.Text = "0"; }
            this.isDirty = true;
        }
        private void txtNoOpin_Leave(object sender, EventArgs e)
        {
            if (txtNoOpin.Text.Trim().Length < 1) { txtSample.Text = "0"; }
            this.isDirty = true;
        }
        #endregion  //====================  End Blanks =================================================================================

        private void btnNew_Click(object sender, EventArgs e)
        {
            int wantsSave = -99;
            if (this.isDirty) {
                wantsSave = AskSaveChanges();
            }
            if (wantsSave < 2)
            {
                lblRecNo.Text = "-1";
               // LoadPollDefaults();
            }
        }

        private void cmbPollOrg_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.isDirty = true;
        }

        private void dtimBegin_ValueChanged(object sender, EventArgs e)
        {
            this.isDirty = true;
            dtimEnd.Value = dtimBegin.Value.AddDays(30);
        }

        private void dtimEnd_ValueChanged(object sender, EventArgs e)
        {
            this.isDirty = true;
        }

        #region //============================  AskSaveChanges ============================
        private int AskSaveChanges()
        {
            int shouldSave = -1;

            DialogResult answr;
            answr = MessageBox.Show("Your changes to this record have not been saved. Do you want to save this record?" +
                "\r\nPlease Click:\r\n" + 
                "\t YES = if you want to save your changes now." + "\r\n" +
                "\t NO = abort the changes and move to another record." + "\r\n" +
                "\t Cancel = if you still want to make changes to this record.","Save Changes?",MessageBoxButtons.YesNoCancel);

            switch (answr)
            { 
                case DialogResult.Yes:
                    btnSave.PerformClick();
                    shouldSave = 0;
                    break;
                case DialogResult.No:
                    shouldSave = 1;                    
                    break;

                default:   //<---- else Cancel
                    shouldSave = 2;
                    break;
            }
            return shouldSave;
        }
        #endregion //============================  END AskSaveChanges ============================


        private void LoadComboBox(ComboBox thisCombo, int SelectedIndx)
        {
            thisCombo.SelectedValue = SelectedIndx;
        }

        private void btnFirst_Click(object sender, EventArgs e)
        {
            int wantsSave = -99;
            if (this.isDirty)
            {
                wantsSave = AskSaveChanges();
            }
            if (wantsSave < 2)
            {
                lblRecNo.Tag = "X";
                lblRecNo.Text = "-99";
            }
        }

        private void btnLast_Click(object sender, EventArgs e)
        {
            int wantsSave = -99;
            if (this.isDirty)
            {
                wantsSave = AskSaveChanges();
            }
            if (wantsSave < 2)
            {
                lblRecNo.Tag = "X";
                lblRecNo.Text = "-11";
            }
        }

        private void btnPrev_Click(object sender, EventArgs e)
        {
            int iPickRec = 0;
            iPickRec = Int32.Parse(txtGoTo.Text);
            int wantsSave = -99;
            if (this.isDirty)
            {
                wantsSave = AskSaveChanges();
            }
            if (wantsSave < 2)
            {
                lblRecNo.Tag = "P";
                lblRecNo.Text = iPickRec.ToString();
                lblRecNo_TextChanged(this,EventArgs.Empty);
            }

        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            int iPickRec = 0;
            iPickRec = Int32.Parse(txtGoTo.Text);
            int wantsSave = -99;
            if (this.isDirty)
            {
                wantsSave = AskSaveChanges();
            }
            if (wantsSave < 2)
            {
                lblRecNo.Tag = "N";
                lblRecNo.Text = iPickRec.ToString();
                lblRecNo_TextChanged(this, EventArgs.Empty);
            }
        }

        private void btnGoto_Click(object sender, EventArgs e)
        {
            int iPickRec = 0;
            iPickRec = Int32.Parse(txtGoTo.Text);
            //iPickRec++;
            lblRecNo.Tag = "X";
            lblRecNo.Text = iPickRec.ToString();
        }

        private void txtGoTo_Enter(object sender, EventArgs e)
        {
            btnSave.Enabled = false;
            ActiveForm.AcceptButton = btnGoto;
            txtGoTo.SelectAll();
            //txtGoTo.SelectionStart = 0;
            //txtGoTo.SelectionLength = txtGoTo.Text.Length;
        }

        private void txtGoTo_Leave(object sender, EventArgs e)
        {
            bool isNumbr = false;
            int iTestNum = 0;
            btnSave.Enabled = true;
            ActiveForm.AcceptButton = btnSave;

            isNumbr = Int32.TryParse(txtGoTo.Text, out iTestNum);
            if (!isNumbr)
            {
                MessageBox.Show("The record number must be numeric", "GoTo not Numeric", MessageBoxButtons.OK);
                btnFirst.PerformClick();
                txtGoTo.Focus();
            }
            else
            {
                if (iTestNum < 1) { btnFirst.PerformClick(); }
            }
        }

        private void txtGoTo_MouseUp(object sender, MouseEventArgs e)
        {
            txtGoTo.SelectAll();
        }

        private void txtNotes_TextChanged(object sender, EventArgs e)
        {
            this.isDirty = true;
        }

        private void txtNotes_Enter(object sender, EventArgs e)
        {
            ActiveForm.AcceptButton = null;
        }

        private void txtNotes_Leave(object sender, EventArgs e)
        {
            ActiveForm.AcceptButton = btnSave;
        }
    }
}
